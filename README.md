# LockYourStuff reminder app

Want to make sure people lock their desktops/laptops/mobiles? Stick this on the screen!

## Build

To buid the production output (serverless files), run

```bash
yarn build
```

## Dev

To develop the app, run

```bash
yarn start
```
