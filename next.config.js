const webpack = require("webpack");
const withTypescript = require("@zeit/next-typescript");
const dotenv = require("dotenv");

const { parsed: localEnv } = dotenv.config();

if (process.env.NODE_ENV !== "production") {
	localEnv.ASSET_PREFIX = process.env.ASSET_PREFIX = "";
}

module.exports = withTypescript({
	target: "server",

	webpack(config) {
		config.plugins.push(new webpack.EnvironmentPlugin(localEnv || {}));

		return config;
	}
});
