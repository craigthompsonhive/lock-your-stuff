import styled, { css } from "styled-components";
import { getStaticUrl } from "../utils/getStaticUrl";

const Container = styled.div`
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1;
	overflow: hidden;
`;

const Blob = css`
	position: absolute;
	width: 68vmin;
	height: 68vmin;
`;

const RedBlob = styled.img`
	${Blob}
	top: -15vmin;
	right: -15vmin;
`;

const YellowBlob = styled.img`
	${Blob}
	bottom: -15vmin;
	left: -15vmin;
`;

export const Background: React.SFC = () => (
	<Container>
		<YellowBlob src={getStaticUrl("images/blobs/yellow.png")} />
		<RedBlob src={getStaticUrl("images/blobs/red.png")} />
	</Container>
);
