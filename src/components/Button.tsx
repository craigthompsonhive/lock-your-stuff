import styled from "styled-components";

export const Button = styled.button`
	display: block;
	padding: 0rem 0.75rem;

	border: 0;
	border-radius: 5px;

	background-color: rgb(253, 208, 15);
	cursor: pointer;

	&:hover,
	&:focus {
		background-color: rgb(243, 198, 05);
	}
`;
