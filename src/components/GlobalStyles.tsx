import { createGlobalStyle } from "styled-components";
import reset from "styled-reset";

export const GlobalStyles = createGlobalStyle`
	${reset}

	@import url('https://fonts.googleapis.com/css?family=Nunito');

	html, body {
		overflow-x: hidden;
	}

	html, body, input, textarea, button {
		font-family: Nunito, sans-serif;
		font-size: 3.5vh;
		color: #474d5e;
	}
`;
