import styled from "styled-components";
import { getStaticUrl } from "../utils/getStaticUrl";

const LogoImage = styled.img.attrs({
	src: getStaticUrl("images/logo.png")
})`
	display: block;
`;

const LogoLink = styled.a.attrs({
	href: `http://hive.hr`
})`
	position: absolute;
	z-index: 100;
	top: 25px;
	left: 25px;
`;

export const Logo: React.SFC = () => (
	<LogoLink>
		<LogoImage />
	</LogoLink>
);
