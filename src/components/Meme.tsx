import { useEffect } from "react";
import styled from "styled-components";
import { ASSET_PREFIX } from "../env";
import { getStaticUrl } from "../utils/getStaticUrl";

interface IPropTypes {
	number: number;
}

const MEME_IMAGE_COUNT = 19;

const Image = styled.div.attrs({})<IPropTypes>`
	position: absolute;
	top: 5%;
	left: 5%;
	width: 90vw;
	height: 90vh;
	z-index: 10;

	background-size: contain;
	background-repeat: no-repeat;
	background-position: 50% 50%;

	background-image: url(${({ number }) =>
		getStaticUrl(`images/memes/${number}.jpg`)});
`;

export const Meme: React.SFC = () => {
	const number = Math.floor(Math.random() * MEME_IMAGE_COUNT + 1);

	// Go full-screen immediately
	useEffect(() => {
		document.documentElement.requestFullscreen();
	});

	return <Image number={number} />;
};
