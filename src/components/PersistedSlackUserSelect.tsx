import { useCallback } from "react";
import { useLocalStorage } from "../hooks/useLocalStorage";
import { ISlackUser } from "../utils/getSlackUsers";
import {
	IProps as ISlackUserSelectProps,
	SlackUserSelect
} from "./SlackUserSelect";

export interface IProps {
	onChange: ISlackUserSelectProps["onChange"];
}

export const PersistedSlackUserSelect: React.SFC<IProps> = props => {
	const [value, setValue] = useLocalStorage<ISlackUserSelectProps["value"]>(
		"slackUsername"
	);

	const onChange = useCallback((option: ISlackUser | undefined) => {
		setValue(option);
		if (props.onChange) {
			props.onChange(option);
		}
	}, []);

	return <SlackUserSelect value={value} onChange={onChange} />;
};
