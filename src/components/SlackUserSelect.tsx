import { memo, useCallback } from "react";
import { components } from "react-select";
import AsyncSelect from "react-select/lib/Async";
import styled, { css } from "styled-components";
import Flex, { FlexItem } from "styled-flex-component";
import { filterSlackUsers } from "../utils/filterSlackUsers";
import { ISlackUser } from "../utils/getSlackUsers";
import { getSlackUsersCached } from "../utils/getSlackUsersCached";
import { Button } from "./Button";

const OptionAvatar = styled.img`
	width: 1.25rem;
	height: 1.25rem;
	border-radius: 5px;
	margin-left: 2px;
	margin-right: 0.5rem;
`;

const NameWithAvatar: React.SFC<ISlackUser> = memo(({ avatar, children }) => (
	<Flex
		inline
		row
		alignCenter
		css={css`
			margin-top: 0.15rem;
		`}
	>
		<FlexItem as={OptionAvatar} grow={0} shrink={0} src={avatar} />
		<FlexItem>{children}</FlexItem>
	</Flex>
));

const Option: React.SFC<any> = memo(({ children, ...props }) => (
	<components.Option {...props}>
		<NameWithAvatar {...props.data}>{children}</NameWithAvatar>
	</components.Option>
));

const SingleValue: React.SFC<any> = memo(({ children, ...props }) => (
	<components.SingleValue {...props}>
		<NameWithAvatar {...props.data}>{children}</NameWithAvatar>
	</components.SingleValue>
));

const NoOptionsMessage: React.SFC<any> = props => (
	<components.NoOptionsMessage {...props}>
		No users found... type something (else)?
	</components.NoOptionsMessage>
);

const COMPONENTS = {
	Option,
	SingleValue,
	NoOptionsMessage
};

export interface IProps {
	value: ISlackUser | undefined;
	onChange: (option: ISlackUser | undefined) => void;
}

const loadOptions = async (inputValue: string) => {
	const users = await getSlackUsersCached();
	return filterSlackUsers(users, inputValue).slice(0, 10);
};

export const SlackUserSelect: React.SFC<IProps> = ({ value, onChange }) => {
	const debouncedLoadOptions = loadOptions;
	const onButtonClick = useCallback(() => onChange(value), [value]);

	// Trigger onChange when we hit enter if we have a value. Useful
	// in cases where the input already has a value and we just want
	// to submit it immediately
	const onKeyDown = useCallback(
		(event: any) => {
			if (event.key === "Enter" && value != null) {
				onChange(value);
			}
		},
		[value]
	);

	return (
		<Flex full contentStretch alignStretch>
			<FlexItem
				as={AsyncSelect}
				tabIndex="1"
				grow={1}
				autoFocus
				defaultOptions
				components={COMPONENTS}
				loadOptions={debouncedLoadOptions}
				value={value}
				onChange={onChange}
				onKeyDown={onKeyDown}
			/>

			<FlexItem
				as={Button}
				tabIndex="2"
				grow={0}
				shrink={0}
				css={css`
					margin-left: 0.5rem;
				`}
				onClick={onButtonClick}
			>
				Go
			</FlexItem>
		</Flex>
	);
};
