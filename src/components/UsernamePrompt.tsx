import styled from "styled-components";
import {
	PersistedSlackUserSelect,
	IProps as ISlackUSerSelectProps
} from "./PersistedSlackUserSelect";
import { OS_FRIENDLY_NAME, detectOs } from "../utils/detectOs";

const Center = styled.section`
	display: flex;
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	overflow: hidden;
	z-index: 1;

	align-items: center;
	justify-content: center;
`;

const Container = styled.div`
	flex: 0 1 auto;
	min-width: 60vmin;
`;

const WhoHeader = styled.h1`
	display: block;
	font-size: 1rem;
	margin: 0 0 1rem;
	text-align: center;
`;

const Tip = styled.p`
	color: #777;
	margin-top: 1rem;
	font-size: 1.75vmin;
	text-align: center;
`;

export interface IProps {
	onChange: ISlackUSerSelectProps["onChange"];
}

export const UsernamePrompt: React.SFC<IProps> = ({ onChange }) => (
	<Center>
		<Container>
			<WhoHeader>
				Who forgot to lock their {OS_FRIENDLY_NAME[detectOs()]}?
			</WhoHeader>
			<PersistedSlackUserSelect onChange={onChange} />
			<Tip>
				PROTIP: Type to search and/or hit the 'Enter' key to select the
				user
			</Tip>
		</Container>
	</Center>
);
