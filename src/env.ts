export const SLACK_OAUTH_TOKEN = process.env.SLACK_OAUTH_TOKEN;

export const SLACK_HOOK_URL = process.env.SLACK_HOOK_URL;

export const ASSET_PREFIX = process.env.ASSET_PREFIX;
