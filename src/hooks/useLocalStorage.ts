import { useState, Dispatch } from "react";

const defaultParser = (value: any) => {
	let json;
	try {
		json = JSON.parse(value);
	} catch (e) {}

	return json;
};

const defaultStringifier = JSON.stringify;

export const useLocalStorage = <T extends any | undefined>(
	key: string,
	initialValue: T | undefined = undefined,
	parser = defaultParser,
	stringifier = defaultStringifier
): [T, Dispatch<T>] => {
	const localStorageUpdater = (newValue: T) =>
		localStorage.setItem(key, stringifier(newValue));

	const [item, setValue] = useState<T>(() => {
		const localStorageValue = localStorage.getItem(key);
		if (localStorageValue == undefined) {
			if (initialValue !== undefined) {
				localStorageUpdater(initialValue);
			}

			return initialValue;
		}

		return parser(localStorageValue);
	});

	const setItem: Dispatch<T> = (newValue: T) => {
		setValue(newValue);
		localStorageUpdater(newValue);
	};

	return [item, setItem];
};
