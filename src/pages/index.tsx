import dynamic from "next/dynamic";
import { useCallback, useState } from "react";
import { Background } from "../components/Background";
import { GlobalStyles } from "../components/GlobalStyles";
import { Logo } from "../components/Logo";
import { Meme } from "../components/Meme";
import { ISlackUser } from "../utils/getSlackUsers";
import { sendSlackMessage } from "../utils/sendSlackMessage";
import React from "react";

// Get rid of these `any`s when the types get fixed
// https://github.com/DefinitelyTyped/DefinitelyTyped/pull/33163
const UsernamePromptNoSSR = dynamic<any>(
	(() =>
		import("../components/UsernamePrompt").then(
			m => m.UsernamePrompt
		)) as any,
	{
		ssr: false,
		loading: () => <div />
	}
);

export const main: React.SFC<any> = () => {
	const [hasEnteredName, setState] = useState(false);
	const onChange = useCallback(
		(option: ISlackUser | undefined) => {
			if (option == null) {
				return;
			}

			sendSlackMessage(option.value);
			setState(true);
		},
		[hasEnteredName]
	);

	return (
		<>
			<GlobalStyles />
			<Background />
			<Logo />

			{hasEnteredName && <Meme />}
			{!hasEnteredName && <UsernamePromptNoSSR onChange={onChange} />}
		</>
	);
};

export default main;
