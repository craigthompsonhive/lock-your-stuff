declare module "styled-flex-component" {
	export const Flex: any;
	export const FlexItem: any;
	export default Flex;
}
declare module "use-debounce/lib/callback" {
	function main<T>(callback: T, duration: number): T;
	export default main;
}
