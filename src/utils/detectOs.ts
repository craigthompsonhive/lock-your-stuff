export enum OS {
	Windows = "Window",
	Mac = "Mac",
	Linux = "Linux",
	Unknown = "Unknown"
}

export const OS_FRIENDLY_NAME = {
	[OS.Windows]: "PC",
	[OS.Mac]: "Mac",
	[OS.Linux]: "device",
	[OS.Unknown]: "device"
};

export const detectOs = (): OS => {
	const { userAgent } = window.navigator;

	if (userAgent.indexOf("Windows") !== -1) {
		return OS.Windows;
	}

	if (userAgent.indexOf("Mac") !== -1) {
		return OS.Mac;
	}

	if (userAgent.indexOf("Linux") !== -1 || userAgent.indexOf("X11") !== -1) {
		return OS.Linux;
	}

	return OS.Unknown;
};
