import { ISlackUser, SlackUsers } from "./getSlackUsers";

const hasCharacters = (inputValue: string, member: string) =>
	member.toLowerCase().includes(inputValue.toLowerCase());

const filterMembers = (inputValue: string, member: ISlackUser) =>
	hasCharacters(inputValue, member.value) ||
	hasCharacters(inputValue, member.label);

export const filterSlackUsers = (users: SlackUsers, filterString: string) => {
	if (filterString == "") {
		return users;
	}

	return users.filter(member => filterMembers(filterString, member));
};
