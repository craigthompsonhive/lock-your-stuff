import orderBy from "lodash/orderBy";
import { SLACK_OAUTH_TOKEN } from "../env";

const USERS_URL = "https://slack.com/api/users.list";

interface ISlackUserListResult {
	ok: boolean;
	members: ISlackMember[];
}

interface ISlackMember {
	id: string;
	name: string;
	is_bot: boolean;
	is_app_user: boolean;
	is_restricted: boolean;
	deleted: boolean;
	profile: {
		real_name: string;
		image_72: string;
	};
}

export interface ISlackUser {
	label: string;
	value: string;
	avatar: string;
}

export type SlackUsers = ISlackUser[];

const getUsername = (member: ISlackMember) => member.name;
const getRealName = (member: ISlackMember) => member.profile.real_name;
const getAvatar = (member: ISlackMember) => member.profile.image_72;

const isBot = (member: ISlackMember) =>
	member.is_bot || member.is_app_user || member.id === "USLACKBOT";

const isRestrictedOrDeleted = (member: ISlackMember) =>
	member.is_restricted || member.deleted;

const filterMembers = (member: ISlackMember) =>
	!isBot(member) && !isRestrictedOrDeleted(member);

export const getSlackUsers = async (limit = 200) => {
	const result = await fetch(
		`${USERS_URL}?token=${SLACK_OAUTH_TOKEN}&limit=${limit}`
	);

	const { ok, members }: ISlackUserListResult = await result.json();
	if (ok !== true) {
		throw new Error(`Could not retrieve users list`);
	}

	const options = members.filter(filterMembers).map(member => ({
		label: getRealName(member),
		value: getUsername(member),
		avatar: getAvatar(member)
	}));

	const sortedOptions = orderBy(options, "label");

	return sortedOptions;
};
