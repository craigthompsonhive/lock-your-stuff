import { once } from "lodash";
import { getSlackUsers } from "./getSlackUsers";

export const getSlackUsersCached = once(getSlackUsers);
