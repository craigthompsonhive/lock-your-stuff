import { ASSET_PREFIX } from "../env";

export const getStaticUrl = (path: string) =>
	`${ASSET_PREFIX || ""}/static/${path}`;
