import { sample } from "lodash";
import { SLACK_HOOK_URL } from "../env";
import { detectOs, OS } from "../utils/detectOs";
import { getSlackUsersCached } from "./getSlackUsersCached";

const SLACK_URL = SLACK_HOOK_URL!;

const OS_NAME = {
	[OS.Windows]: ":computer:",
	[OS.Mac]: ":macbookpro:",
	[OS.Linux]: "laptop :computer:",
	[OS.Unknown]: ":computer:"
};

const HASH_TAGS = [
	"harriane-death-stare",
	"isofail",
	"isosilly",
	"gdpr4life",
	"hurricane-no-happy",
	"securityplz",
	"cybershame"
];

const FORFEIT_GENERATORS = [
	(username: string) => `Get <@${username}> some sweets :lollipop:`,
	(username: string) => `Buy <@${username}> a drink :wine: :beer: :wylam:`,
	(username: string) => `Treat <@${username}> to a hot drink :coffee:`,
	(username: string) => `Give <@${username}> a hug :hugging_face:`,
	(username: string) => `Buy <@${username}> some cake :cake: :birthday:`,
	(username: string) => `Treat <@${username}> to a Greggs :greggs:`,
	(username: string) => `Get <@${username}> a gift :gift: :hushed:`,
	(username: string) => `Buy <@${username}> some flowers :bouquet: :rose:`,
	(username: string) => `Treat <@${username}> to a cookie :cookie:`,
	(username: string) => `Buy <@${username}> some chocolate :chocolate_bar:`,
	(username: string) => `Buy <@${username}> some ice cream :icecream:`
];

const generateMessage = (username: string, forfeit: string, hashtags: string) =>
	`<@${username}> You forgot to lock your ${
		OS_NAME[detectOs()]
	}! ${forfeit} ${hashtags}`;

export const sendSlackMessage = async (username: string) => {
	const users = await getSlackUsersCached();
	const randomUser = sample(users)!;
	const forfeit = sample(FORFEIT_GENERATORS)!(randomUser.value);

	const hashtags = `#forfeit #${sample(HASH_TAGS)}`;

	const payload = {
		text: generateMessage(username, forfeit, hashtags)
	};

	fetch(SLACK_URL, {
		method: "POST",
		body: JSON.stringify(payload)
	});
};
